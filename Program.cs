﻿using System;

namespace week3_ex15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number to multiply by 5");
            var userNumber = int.Parse (Console.ReadLine());

            Console.WriteLine($"Your number multiplied by 5 equals {userNumber * 5}");
        }
    }
}
